$(document).ready(function(){
  $('.slider').slick({
    arrows:true,
    dots:true,
    slidesToShow:2,
    adaptiveHeigh:true,
    variableWidth:true,
    
    
  });
});


const prev=document.getElementById('btn-prevs'),
      next=document.getElementById('btn-nexts'),
      slides=document.querySelectorAll('.slider2'),
      dots=document.querySelectorAll('.dotted');
      let index = 0;
      const activeSlide = n =>{
          
        for(let slider2 of slides){
            slider2.classList.remove('active')
             
        }
        slides[n].classList.add('active');
        

      }
      const activeDot = n =>{
        console.log(n);
      for(let dotted of dots){
          dotted.classList.remove('active')
           
      }
      dots[n].classList.add('active');

    }
    const prepareCurrentSlide =ind =>{
      activeSlide(ind);
      activeDot(ind);

    }

// second slider
      const nextSlide = () =>{
          if(index==slides.length-1){
          index=0;
         activeSlide(index);
         activeDot(index);
         prepareCurrentSlide(index)

          }else
          index++;
          activeSlide(index);
          prepareCurrentSlide(index)
      };
      const prevSlide = () =>{
        if(index==0){
        index=slides.length-1;
       activeSlide(index);
       activeDot(index)
       prepareCurrentSlide(index)

        }else
        index--;
        activeSlide(index);
        prepareCurrentSlide(index)

    };
    dots.forEach((item,indexDot) =>{
      item.addEventListener('click',()=>{

     
      index=indexDot;
      prepareCurrentSlide(index);

    })
  });

      next.addEventListener('click',nextSlide);
      prev.addEventListener('click',prevSlide)

//hamburger menu
const menu_btn=document.querySelector('.hamburger');
const mobile_menu=document.querySelector('.mobile-nav');

menu_btn.addEventListener('click',function(){
  menu_btn.classList.toggle('is-active');
  mobile_menu.classList.toggle('is-active');
});